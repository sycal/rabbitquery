#!/usr/bin/env python

import argparse
import base64
import gevent
import json
import operator
import os
import re
import sys
import time
import urllib
from ConfigParser import SafeConfigParser
from gevent.pool import Pool
from geventhttpclient import HTTPClient
from StringIO import StringIO

ACTION_LIST_QUEUES = 'list'
ACTION_PURGE_QUEUES = 'purge'
ACTION_DELETE_QUEUES = 'delete'
ACTION_TIME_QUEUES = 'time'

ALL_ACTIONS = [ACTION_LIST_QUEUES, ACTION_PURGE_QUEUES, ACTION_DELETE_QUEUES, ACTION_TIME_QUEUES]

SCRIPT_NAME = os.path.splitext(os.path.basename(sys.argv[0]))[0]
DEFAULTS_FILE = '~/.{0}'.format(SCRIPT_NAME)


class AbortError(Exception):
    pass


class Runner(object):

    CONCURRENCY = 10
    DEFAULT_CONNECTION_TIMEOUT = 30
    DEFAULT_NETWORK_TIMEOUT = 60

    def __init__(self, server, port, vhost, user, password, connection_timeout=None, network_timeout=None):
        self.server = server
        self.vhost = vhost
        self._client = HTTPClient(
            server,
            port,
            concurrency=self.CONCURRENCY,
            connection_timeout=connection_timeout or self.DEFAULT_CONNECTION_TIMEOUT,
            network_timeout=network_timeout or self.DEFAULT_NETWORK_TIMEOUT,
        )
        self._headers = {}
        self._headers = {'Referer': self.server, 'Content-Type': 'application/json'}
        if user and password:
            token = '{0}:{1}'.format(user, password)
            self._headers['Authorization'] = 'Basic {0}'.format(base64.encodestring(token).strip())

    def list_queues(self, action_queue=None, verbose_level=0, quiet=False, hide_empty=False):
        response_data = self._list_queues()
        queue_data_by_name = {i['name']: i for i in response_data
                              if not action_queue or self._match(i['name'], action_queue)}
        total_messages = sum(i['messages'] for i in queue_data_by_name.values())
        sorted_queue_names = sorted(queue_data_by_name.keys())
        queue_num = 0
        for queue in sorted_queue_names:
            queue_data = queue_data_by_name[queue]
            message_count = queue_data['messages']
            if hide_empty and message_count == 0:
                continue
            queue_num += 1
            if verbose_level:
                if verbose_level > 1:
                    print "{0} : messages={1} durable={2} memory={3}k".format(
                        queue,
                        message_count,
                        queue_data['durable'],
                        int(queue_data['memory'] / 1024),
                    )
                else:
                    print "{0} : messages={1}".format(
                        queue,
                        message_count,
                    )
            else:
                print queue
        if not quiet:
            if action_queue or queue_num != 1:
                tot = " (total messages={0})".format(total_messages) if verbose_level else ""
                prefix = 'Matched ' if action_queue else ''
                print "{0}{1} queue{2}{3}".format(prefix, queue_num, '' if queue_num == 1 else 's', tot)

    def _list_queues(self):
        url = '/api/queues/{0}'.format(urllib.quote_plus(self.vhost))
        response = self._client.get(url, headers=self._headers)
        if response.status_code != 200:
            raise AbortError("{0} {1}".format(response.status_code, response.status_message))
        response_data = json.loads(response.read())
        return response_data

    def purge_queues(self, action_queue=None, auto_yes=False, verbose_level=0):
        response_data = self._list_queues()
        if action_queue:
            queue_list = [i['name'] for i in response_data
                          if i['messages'] > 0 and self._match(i['name'], action_queue)]
        else:
            queue_list = [i['name'] for i in response_data if i['messages'] > 0]
        queue_num = len(queue_list)
        if action_queue or verbose_level:
            for queue in queue_list:
                print "Matched : {0}".format(queue)
        if queue_num and not auto_yes and not self._agree("Purge {0} queue{1}? (y/N) ".format(
                queue_num, '' if queue_num == 1 else 's')):
            raise AbortError("Aborted!")
        pool = Pool(self.CONCURRENCY)
        for queue in queue_list:
            uri = '/api/queues/{0}/{1}/contents'.format(urllib.quote_plus(self.vhost), queue)
            pool.spawn(self._client.delete, uri, headers=self._headers)
        pool.join()
        if queue_num == 0:
            print "Nothing to purge"
        else:
            print "Purged {0} queue{1}".format(queue_num, '' if queue_num == 1 else 's')

    def delete_queues(self, action_queue=None, auto_yes=False, verbose_level=0):
        response_data = self._list_queues()
        if action_queue:
            queue_list = [i['name'] for i in response_data if self._match(i['name'], action_queue)]
        else:
            queue_list = [i['name'] for i in response_data]
        queue_num = len(queue_list)
        if action_queue or verbose_level:
            for queue in queue_list:
                print "Matched : {0}".format(queue)
        if queue_num and not auto_yes and not self._agree("Delete {0} queue{1}? (y/N) ".format(
                queue_num, '' if queue_num == 1 else 's')):
            raise AbortError("Aborted!")
        pool = Pool(self.CONCURRENCY)
        for queue in queue_list:
            uri = '/api/queues/{0}/{1}'.format(urllib.quote_plus(self.vhost), queue)
            pool.spawn(self._client.delete, uri, headers=self._headers)
        pool.join()
        if queue_num == 0:
            print "Nothing to delete"
        else:
            print "Deleted {0} queue{1}".format(queue_num, '' if queue_num == 1 else 's')

    def time_queues(self, action_queue, message_count, verbose_level=0):
        def get_queue_data():
            response_data = self._list_queues()
            if action_queue:
                return {i['name']: i['messages'] for i in response_data if self._match(i['name'], action_queue)}
            else:
                return {i['name']: i['messages'] for i in response_data}

        queues_by_name = get_queue_data()
        queue_num = len(queues_by_name)
        if action_queue or verbose_level:
            for queue in queues_by_name:
                print "Matched : {0} : messages={1}".format(queue, queues_by_name[queue])
        else:
            print "Matched {0} queue{1}".format(queue_num, self._pl(queue_num))
        if queue_num:

            initial_total_messages = sum(queues_by_name.values())
            if initial_total_messages != message_count:
                compare_fn = operator.le if initial_total_messages > message_count else operator.ge
                print "Timing until messages in all matched queues equal {0}".format(message_count)
                start_time = time.time()
                while True:
                    queues_by_name = get_queue_data()
                    total_messages = sum(queues_by_name.values())
                    if verbose_level:
                        print "Total messages={0}".format(total_messages)
                    if compare_fn(total_messages, message_count):
                        break
                    gevent.sleep(1)
                diff_time = int(time.time() + 0.5 - start_time)
                diff_messages = abs(initial_total_messages - total_messages)
                mps = int(diff_messages / diff_time)
                print "Took {0}s {1}mps".format(diff_time, mps)
            else:
                print "Message count is already {0}".format(message_count)

    def close(self):
        if self._client:
            self._client.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        del exc_type, exc_val, exc_tb
        self.close()

    @staticmethod
    def _agree(text):
        reply = raw_input(text)
        return reply.lower() in ['y', 'yes']

    @staticmethod
    def _match(queue, test_queue):
        return re.search(test_queue, queue)

    @staticmethod
    def _pl(count):
        return '' if count == 1 else 's'


def get_args():
    conf_parser = argparse.ArgumentParser(add_help=False)
    conf_parser.add_argument('--defaults-file', help="Path to the defaults file (default {0}).".format(DEFAULTS_FILE))
    args, remaining_argv = conf_parser.parse_known_args()

    parser = argparse.ArgumentParser(
        description="Performs multi-queue operations on rabbitmq such as listing, purging and deleting .",
        parents=[conf_parser]
    )
    parser.add_argument('action', help="Sets the action to perform.", choices=ALL_ACTIONS)
    parser.add_argument('queue', help="The queue pattern to perform the action upon.", nargs='?')
    parser.add_argument('-s', '--server', help="The rabbitmq server.", default='localhost')
    parser.add_argument('-p', '--port', type=int, help="The rabbitmq management port (default 15672).", default=15672)
    parser.add_argument('-c', '--credentials', help="The rabbitmq credentials (user:password)", default='')
    parser.add_argument('-v', '--verbose', help="Verbose output (-vv for extra info).", action='count', default=0)
    parser.add_argument('-y', '--yes', help="Implicitly set answers to 'yes'.", action='store_true')
    parser.add_argument('--vhost', help="The vhost to use.", default='/')
    parser.add_argument('-q', '--quiet', help="Do not display list summary info.", action='store_true')
    parser.add_argument('-e', '--hide-empty', help="Do not list empty queues.", action='store_true')
    parser.add_argument('-ct', '--connect-timeout', type=int,
                        help="Connection timeout seconds (default {}).".format(Runner.DEFAULT_CONNECTION_TIMEOUT))
    parser.add_argument('-nt', '--network-timeout', type=int,
                        help="Network timeout seconds (default {}).".format(Runner.DEFAULT_NETWORK_TIMEOUT))
    parser.add_argument('-mc', '--message-count', type=int, default=0,
                        help="Message count to reach for timing action (default 0).")

    if args.defaults_file and not os.path.isfile(args.defaults_file):
        raise AbortError("Invalid defaults file path!")

    defaults_file_name = args.defaults_file or DEFAULTS_FILE
    default_file_path = os.path.expanduser(defaults_file_name)
    if os.path.isfile(default_file_path):
        config = SafeConfigParser()
        with open(os.path.expanduser(DEFAULTS_FILE)) as stream:
            stream = StringIO("[defaults]\n" + stream.read())
            config.readfp(stream)
        defaults_dict = dict(config.items('defaults'))
        parser.set_defaults(**defaults_dict)

    return parser.parse_args(remaining_argv)


def main():
    cl_args = get_args()
    user, _, password = cl_args.credentials.partition(':')
    with Runner(
        server=cl_args.server,
        port=cl_args.port,
        vhost=cl_args.vhost,
        user=user,
        password=password,
        connection_timeout=cl_args.connect_timeout,
        network_timeout=cl_args.network_timeout,
    ) as runner:
        if cl_args.action == ACTION_LIST_QUEUES:
            runner.list_queues(action_queue=cl_args.queue, verbose_level=cl_args.verbose, quiet=cl_args.quiet,
                               hide_empty=cl_args.hide_empty)
        elif cl_args.action == ACTION_PURGE_QUEUES:
            runner.purge_queues(action_queue=cl_args.queue, auto_yes=cl_args.yes, verbose_level=cl_args.verbose)
        elif cl_args.action == ACTION_DELETE_QUEUES:
            runner.delete_queues(action_queue=cl_args.queue, auto_yes=cl_args.yes, verbose_level=cl_args.verbose)
        elif cl_args.action == ACTION_TIME_QUEUES:
            runner.time_queues(action_queue=cl_args.queue, message_count=cl_args.message_count,
                               verbose_level=cl_args.verbose)

if __name__ == '__main__':
    try:
        main()
    except AbortError as e:
        print e.message
    except KeyboardInterrupt:
        pass
    finally:
        gevent.sleep(0.1)
