# RabbitQuery

A utility to list, purge and delete multiple rabbitmq queues.
Queues are matched using regular expressions.

Credentials and other parameters can be set in a configuration
file as well as passed in via the command line.

## Examples:

List all queues, including messages counts:

    rabbitquery.py list -v

List all queues containing the text 'performance', including counts and memory usage:

    rabbitquery.py list performance -vv

Delete all queues (will require confirmation):

    rabbitquery.py delete 

Purge all queues that start with 'results' (no confirmation):

    rabbitquery.py purge ^results -y

Time how long until all performance queues are empty:

    rabbitquery.py time performance

Time how long to reach 100,000 messages across all the performance queues, displaying current total count each second:

    rabbitquery.py time performance -mc 100000 -v


## Optional configuration file:

The name of the configuration file depends on the name of the 
script. Using the '-h' option to view the help shows 
the actual name of the file used.

The contents of the configuration file can also be inferred
from the help options; for example, to set the credentials,
the file would just contain something like:

    credentials=username:password

